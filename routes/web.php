<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//REg Auth
Route::get('aun', array('uses' => 'UserController@validateUsername'));
Route::post('aun', array('uses' => 'UserController@postValidateUsername'));
Route::get('aun-step2',array('uses' => 'UserController@getRegStep2'));
Route::get('aun-step3',array('uses' => 'UserController@getRegStep3'));
Route::post('aun-step2',array('uses' => 'UserController@validateStep2'));
Route::post('aun-step3',array('uses' => 'UserController@validateStep3'));
Route::get('support-noauth', function(){
     return view('admin.reg_error');
});
Route::get('404',function(){
   return view('error_404'); 
});
Route::get('registration-success',function(){
    Auth::logout();
    return View::make('admin.reg_success');
});
Route::post('save-location','AuthController@postSaveLocation');




Route::group(['middleware' => 'guest'], function()
{
Route::get('/', function () {        
        return view('welcome');
    });
    Route::get('login', array('uses' => 'AuthController@showLogin','as'=>'login'));
    Route::post('post-login', array('uses' => 'AuthController@doLogin'));
});

    Route::group(['middleware' => ['auth']], function () {
    Route::get('logout', array('uses' => 'AuthController@logout'));
    Route::get('dashboard',array('uses'=>'DashboardController@index'));
    Route::get('profile',array('uses'=>'ProfileController@myProfile'));
    Route::get('updateprofile',array('uses'=>'ProfileController@updateProfile'));
    Route::post('updateprofile/{id}',array('uses'=>'ProfileController@postProfile'));
    Route::get('activity-logs',array('uses'=>'ActivityLogController@index'));
    Route::get('activity-logs/{id}',array('uses'=>'ActivityLogController@show'));
    Route::get('changepassword',array('uses'=>'ProfileController@getChangePassword'));
    Route::post('changepassword',array('uses'=>'ProfileController@postChangePassword'));
    Route::get('current-passcheck',array('uses'=>'ProfileController@checkCurrentPassword'));
    Route::get('users',array('uses'=>'UserController@index'));
    Route::get('user-keys',array('uses'=>'UserController@getUserKeys'));
    Route::post('send-request',array('uses'=>'UserController@sendUserRequest'));
    Route::group(['prefix'=>'user'],function(){
       Route::get('view-edit/{id}','UserController@getEditView');
       Route::get('update/{id}','UserController@getUpdateView');
       Route::post('update/{id}','UserController@postUpdateUser');
       Route::get('new/','UserController@newUser');
    });
});