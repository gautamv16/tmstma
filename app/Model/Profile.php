<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

    protected $table = 'user_info';
    public $timestamps = true;

    public function address() {
        return $this->belongsTo('App\Model\Address', 'address_id');
    }
    
    public function role(){
        return $this->belongsTo('App\Model\UserTypes', 'user_type');
    }

}
