<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    protected $table = "site_logs";
    public $timestamps = true;

    protected $fillable = ['cid','user_id','record_id','record_cid','old_data','ip','location','action_type','created_at','updated_at'];

     public function user(){
         return $this->belongsTo('App\Model\User','user_id');
     }
}
