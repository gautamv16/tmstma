<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    public function profile() {
        return $this->hasOne('App\Model\Profile', 'user_id');
    }

    public function address() {
        return $this->hasOne('App\Model\Address', 'user_id');
    }

    public static function getAllUsers() {
        return self::with('profile.role')->get();
    }

    public static function getUser($id) {
        return self::with(['profile.role', 'address'])->findOrFail($id);
    }

}
