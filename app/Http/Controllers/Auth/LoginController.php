<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{   
    protected $redirectTo = '/dashboard';
    protected $username = 'login_id';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLogin(){
       return View::make('welcome');
    }

    public function doLogin(){
         $data = $request->all();

        $validator = Validator::make($data, [
                    $this->loginUsername() => 'required',
                    'password' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to('login')
                    ->withErrors($validator) // send back all errors to the login form
                    ->withInput(Input::except('password'));
            
        }

        
        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {

        }else{
            return Redirect::to('login');
        }
    }

    public function loginUsername(){
        return property_exists($this, 'username') ? $this->username : 'login_id';
    }

    protected function getCredentials(Request $request) {
        return $request->only($this->loginUsername(), 'password');
    }

}
