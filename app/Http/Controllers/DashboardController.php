<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Validator;
use App\Model\User;
use Auth;
use View;
use Illuminate\Support\Facades\Input;

class DashboardController extends Controller
{
    
   public function index(){      
       return View::make('admin.dashboard');
   }
}
