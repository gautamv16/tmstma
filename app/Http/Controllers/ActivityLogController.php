<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Validator;
use App\Model\User;
use Auth;
use View;
use Illuminate\Support\Facades\Input;
class ActivityLogController extends Controller
{
    public function index(){
        $logs = \LogActivity::logActivityLists();
        return view('admin.activity_logs',['logs'=>$logs]);
    }

    public function show($id){
        $logData = \LogActivity::logActivity($id);
        return view('admin.activity_logs',['log'=>$logData]);
    }
}
