<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Validator;
use App\Model\User;
use Auth;
use View;
use Illuminate\Support\Facades\Input;

class AuthController extends Controller {

    protected $redirectTo = '/dashboard';
    protected $username = 'login_id';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout', 'save-location');
    }

    public function showLogin() {
        if (Auth::check()) {
            return redirect('dashboard');
        }
        return View::make('welcome');
    }

    public function doLogin(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
                    $this->loginUsername() => 'required',
                    'password' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to('login')
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(Input::except('password'));
        }
        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            //location
            \LocationIp::save($data);
            //$this->saveLocation($data);

            //logactivity
            \LogActivity::addToLog('Login', Auth::check() ? Auth::user()->id : null, Auth::check() ? Auth::user()->cid : null);
            session(['login_time' => date('h:i:s')]);
            session(['login_date' => date('d-M-Y')]);

            return Redirect::to('dashboard');
        } else {
            return redirect()->back()->with('error', 'Invalid credentials!');
        }
    }

    public function logout() {
        //logactivity
        \LogActivity::addToLog('Logout', Auth::check() ? Auth::user()->id : null, Auth::check() ? Auth::user()->cid : null);
        Auth::logout();
        session()->forget('login_time');
        session()->forget('login_date');
        return redirect('/login');
    }

    public function loginUsername() {
        return property_exists($this, 'username') ? $this->username : 'login_id';
    }

    protected function getCredentials(Request $request) {
        return $request->only($this->loginUsername(), 'password');
    }

   /* public function saveLocation($all) {
        session(['ip' => $all['ip'], 'city' => $all['city'], 'region' => $all['region'], 'country'
            => $all['country']]);
        return;
    }*/

    public function postSaveLocation(Request $request) {
        $all = $request->all();
        session(['ip' => $all['ip'], 'city' => $all['city'], 'region' => $all['region'], 'country'
            => $all['country']]);
        session()->save();
        return true;
    }

}
