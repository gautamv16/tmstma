<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Validator;
use App\Model\User;
use Auth;
use View;
use Illuminate\Support\Facades\Input;
use App\Model\UserTypes;
use App\Model\Profile;
use App\Model\Address;
use App\Model\Contact;
use Mail;
use Hash;
use Illuminate\Support\Facades\Config;

class UserController extends Controller {

    public function index() {
        $types = UserTypes::where('status', 1)->get();
        $users = User::getAllUsers();
        return View::make('admin.users', ['types' => $types, 'users' => $users]);
    }

    public function getUserKeys(Request $request) {
        $all = $request->all();
        $cid = $this->generateCID($all);
        $username = $this->generateUsername($all);
        return response()->json(['cid' => $cid, 'username' => $username]);
    }

    public function generateCID($all) {
        $fn2 = substr($all['first_name'], 0, 2);
        $ln2 = substr($all['last_name'], 0, 2);
        $month = date('m');
        $hour = date('H');
        $cid = strtoupper($fn2) . strtoupper($ln2) . $month . $hour . $this->randomNumbers(3);
        //check unique
        $res = User::where('cid', '=', $cid)->count();
        if ($res) {
            return $this->generateCID($all);
        }
        return $cid;
    }

    public function generateUsername($all) {
        $fn = substr($all['first_name'], 0, 1);
        $lname = $all['last_name'];
        $username = $fn . $lname;
        $res = User::where('login_id', '=', $username)->count();
        if ($res) {
            $fn = substr($all['first_name'], 0, 2);
            $username = $fn . $lname;
            $res = User::where('login_id', '=', $username)->count();
            if ($res) {
                return $this->getUniqueUsername($username, 2);
            }
        }
        return strtolower($username);
    }

    public function getUniqueUsername($username, $digit) {
        $uUsername = $username . $digit;
        $res = User::where('login_id', '=', $uUsername)->count();
        if ($res) {
            $digit++;
            return $this->getUniqueUsername($username, $digit);
        }
        return $uUsername;
    }

    public function randomNumbers($digits = 3) {
        return str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
    }

    public function sendUserRequest(Request $request) {
        $all = $request->all();
        $email = $all['email'];

        $validator = Validator::make($all, [
                    'login_id' => 'required|unique:users',
                    'cid' => 'required|unique:users',
                    'email' => 'required|email|unique:user_info'
        ]);
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput();
        }
        $user = new User();
        $user->cid = $all['cid'];
        $user->login_id = $all['login_id'];
        $user->save();

        //user info
        $profile = new Profile();
        $profile->cid = $all['cid'];
        $profile->user_id = $user->id;
        $profile->first_name = $all['first_name'];
        $profile->last_name = $all['last_name'];
        $profile->middle_name = $all['middle_name'];
        $profile->suffix = $all['suffix'];
        $profile->email = $all['email'];
        $profile->mobile = $all['mobile'];
        $profile->user_type = $all['user_type'];
        $profile->instrument = isset($all['instrument']) ? $all['instrument'] : '';
        $profile->backline = isset($all['backline']) ? $all['backline'] : '';
        $profile->client_association = isset($all['client_association']) ? $all['client_association'] : '';
        $profile->save();

        //save address info
        $address = new Address();
        $address->user_id = $user->id;
        $address->cid = $user->cid;
        $address->address1 = $all['address1'];
        $address->address2 = $all['address2'];
        $address->city = $all['city'];
        $address->state = $all['state'];
        $address->country = $all['country'];
        $address->zip_code = $all['zip_code'];
        $address->status = 1;
        $address->save();
        //update address in user
        $profile->address_id = $address->id;
        $profile->update();

        //contact table
        $contact = New Contact();
        $contact->user_id = $user->id;
        $contact->creator_user_id = Auth::user()->id;
        $contact->contact_type = $all['user_type'];
        $contact->status = 0;
        $contact->save();

        $data['first_name'] = $all['first_name'];
        $data['username'] = $all['login_id'];
        $data['link'] = url('aun?q=' . base64_encode($all['login_id']));
        Mail::send('emails.validate_username', ['data' => $data], function($message) use($email) {
            $message->to($email)
                    ->subject('User Request Verification');
        });
        //activity log
        \LogActivity::addToLog('NewUserReg',$user->id,$user->cid);
        session()->flash('success', 'Registration initiated successfully. Also email has been sent to user.');
        return redirect('/users');
    }

    public function validateUsername(Request $request) {
        $uname = base64_decode($request->get('q'));
        $user = User::with('profile')->where('login_id', '=', $uname)->first();
        session()->has('un_failed_attempt') ? session(['un_failed_attempt' => session('un_failed_attempt')]) : session(['un_failed_attempt' => 0]);
       
        
        if (!$user->count()) {
            session()->flash('message', 'Invalid Link');
            return Redirect::to('support-noauth');
        } else {
            if ($this->checkLinkExpired($user)) {
                if(!$user->status){
                    $msg = 'Link expired, you will recieve another email from admin.';
                }else{
                    $msg = "Link has been expired";
                }
                session()->flash('message', $msg);
                return Redirect::to('support-noauth');
            }          

            if ($user->profile->reg_comp_date != '') {  
                session()->flash('message', 'Link Expired! you are already registered');
                return Redirect::to('support-noauth');
            }
            if ($user->failed_attempt == 2) {
                session()->flash('message', 'No more login attempts available. Please be patient we will be back to you soon.');
                return Redirect::to('support-noauth');
            }
            
            
        }
        return View::make('admin.reg_step1', ['uname' => $request->get('q')]);
    }

    public function postValidateUsername(Request $request) {
        $all = $request->all();
        $uname = base64_decode($all['uname']);
        $username = $all['username'];
        $user = User::with('profile')->where('login_id', '=', $uname)->first();
         \LocationIp::save($request->all());
        $validator = Validator::make($all, [
                    'username' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to form
                            ->withInput();
        }

        if ($uname != $username) {
            return Redirect::back()->with(['error' => 'Invalid username, please enter correct username.']);
            /*$isAtmpt = $this->checkFailedAttempts($user);
            if ($isAtmpt && $isAtmpt > 0) {
                return Redirect::back()->with(['error' => 'Invalid username! you have ' . $isAtmpt . ' more attempt.']);
            } else {
                
                
                session()->flash('message', 'You have exceeded the attempt limit to verify your account thus and this registration process has been cancelled. You will be contacted by site managers from The Management Ark as how to proceed. Please delete the email that contains this link.');
                return Redirect::to('support-noauth');
            } */
        }

        if ($this->checkLinkExpired($user)) {
            session()->flash('message', 'Link expired, you will recieve another email from admin.');
            return Redirect::to('support-noauth');
        }

        //Send 6 digit code & move to next step
        $authCode = mt_rand(100000, 999999);
        $data['first_name'] = $user->profile->first_name;
        $data['code'] = $authCode;
        $email = $user->profile->email;

        $profile = $user->profile;
        $profile->auth_code = $authCode;
        $profile->authcode_sent_date = date('y-m-d H:i:s');
        $profile->update();

        Mail::send('emails.verification_code', ['data' => $data], function($message) use($email) {
            $message->to($email)
                    ->subject('One Time Verification Code');
        });

        return Redirect::to('aun-step2')->with(['user' => $user]);

        // return View::make('admin.reg_step2', ['user' => $user]);
    }

    public function getRegStep2() {
        $user = session()->get('user');
        if (empty($user)) {
            session()->flash('message', 'Code expired! please follow the link again.');
            return Redirect::to('support-noauth');
        }
        return View::make('admin.reg_step2', ['user' => $user]);
    }

    public function validateStep2(Request $request) {
        $all = $request->all();
        $id = $all['user_id'];
        $code = $all['code'];
        $user = User::with('profile')->where('id', '=', $id)->first();
        $validator = Validator::make($all, [
                    'code' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                            ->with(['user' => $user])
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput();
        }
        if ($code != $user->profile->auth_code) {
            session()->flash('error', 'Invalid Code');
            return Redirect::back()->with(['user' => $user]);
        }
        //auth response date
        $profile = $user->profile;
        $profile->auth_res_date = date('y-m-d H:i:s');
        $profile->auth_code = '';
        $profile->update();
        return Redirect::to('aun-step3')->with(['user' => $user]);
    }

    public function getRegStep3() {
        $user = session()->get('user');
        return View::make('admin.reg_step3', ['user' => $user]);
    }

    public function validateStep3(Request $request) {
        $all = $request->all();
        $id = $all['user_id'];
        $user = User::with('profile')->where('id', '=', $id)->first();
        if (!$user->count()) {
            session()->flash('message', 'Registration Failed! invalid user request.');
            return Redirect::to('support-noauth');
        }
        $validator = Validator::make($request->all(), [
                    'password' => 'required',
                    'confirm_password' => 'required|same:password'
        ]);
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput()->with(['user'=>$user]);
        }

        $password = Hash::make($request->get('password'));
        $user->password = $password;
        $user->pwd_assign_date = date('y-m-d H:i:s');
        $user->status = 1;
        $user->update();

        $profile = $user->profile;
        $profile->reg_comp_date = date('y-m-d H:i:s');
        $profile->update();

        //activity log
        \LogActivity::addToLog('CmpltUserReg',$user->id,$user->cid);
        return Redirect::to('registration-success/');
    }

    public function checkFailedAttempts($user) {

        $fatmpt = $user->failed_attempt;
        $fatmpt++;
        $user->failed_attempt = $fatmpt;
        $user->update();
        if ($fatmpt == 3) {
            
            $user->status = 2;
            $user->update();
            //activity log
            \LogActivity::addToLog('NUserRegFail',$user->id,$user->cid);
            //notify  admin
            Mail::send('emails.admin_new_reg_failed', ['user' => $user], function($message) {
                $message->to(Config::get('app.adminmail'))->bcc('reenanalwa.shoponsteelbird@gmail.com')
                        ->subject(__('messages.NUserRegFail'));
            });
        }
        return (2 - $fatmpt);
    }

    public function checkLinkExpired($user) {
        $now = date('y-m-d H:i:s');
        $createAt = $user->created_at;

        $hourdiff = round((strtotime($now) - strtotime($createAt)) / 3600, 1);
        if ($hourdiff > 48) {            
            if($user->status == 0){
                $user->status = 2;
                $user->update();
                //activity log
                \LogActivity::addToLog('NUserRegFail',$user->id,$user->cid);

                //notify  admin
                Mail::send('emails.admin_new_reg_failed', ['user' => $user], function($message) {
                    $message->to(Config::get('app.adminmail'))->bcc('reenanalwa.shoponsteelbird@gmail.com')
                            ->subject(__('messages.NUserRegFail') . ' - Link Expired');
                });
            }
            return true;
        }
        return false;
    }

    public function getEditView($id) {
        $types = UserTypes::where('status', 1)->get();
        $users = User::getAllUsers();
        $userInfo = User::getUser($id);
        return View::make('admin.user_edit', ['types' => $types, 'users' => $users, 'userInfo' => $userInfo]);
    }

    public function getUpdateView($id) {
        $types = UserTypes::where('status', 1)->get();
        $users = User::getAllUsers();
        $userInfo = User::getUser($id);
        return View::make('admin.user_update', ['types' => $types, 'users' => $users, 'userInfo' => $userInfo]);
    }

    public function postUpdateUser(Request $request, $id) {

        $all = $request->all();
        $user = User::getUser($id);
        $profile = $user->profile;

        $validator = Validator::make($all, [
                    'login_id' => 'required|unique:users,login_id,' . $user->id,
                    'cid' => 'required|unique:users,cid,' . $user->id,
                    'email' => 'required|unique:user_info,email,' . $profile->id
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput();
        }

        //user info
        $profile->cid = $all['cid'];
        $profile->user_id = $user->id;
        $profile->first_name = $all['first_name'];
        $profile->last_name = $all['last_name'];
        $profile->middle_name = $all['middle_name'];
        $profile->suffix = $all['suffix'];
        $profile->email = $all['email'];
        $profile->mobile = $all['mobile'];
        $profile->user_type = $all['user_type'];
        $profile->instrument = isset($all['instrument']) ? $all['instrument'] : '';
        $profile->backline = isset($all['backline']) ? $all['backline'] : '';
        $profile->client_association = isset($all['client_association']) ? $all['client_association'] : '';
        $profile->update();

        //save address info
        $address = $user->address;
        $address->user_id = $user->id;
        $address->cid = $user->cid;
        $address->address1 = $all['address1'];
        $address->address2 = $all['address2'];
        $address->city = $all['city'];
        $address->state = $all['state'];
        $address->country = $all['country'];
        $address->zip_code = $all['zip_code'];
        $address->status = 1;
        $address->update();

        session()->flash('success', 'User ' . $user->login_id . ' with CID:  ' . $user->cid . ' Updated Successfully.');
        return Redirect::to('/user/view-edit/'.$user->id);
    }
    
    public function newUser(){
        $types = UserTypes::where('status', 1)->get();
        $users = User::getAllUsers();
        return View::make('admin.new_user', ['types' => $types, 'users' => $users]);
    }

}
