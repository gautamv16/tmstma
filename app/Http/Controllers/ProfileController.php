<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Validator;
use App\Model\User;
use Auth;
use View;
use Illuminate\Support\Facades\Input;
use App\Model\Address;
use App\Model\Profile;
use Hash;

class ProfileController extends Controller {

    public function myProfile() {
        if (Auth::check()) {
            $id = Auth::user()->id;
            $user = User::with(['profile.address', 'profile.role'])->where('id', '=', $id)->first();
            //$address = Address::where('user_id','=', $id)->where('status','=','1')->first();
            return View::make('admin.myprofile', ['user' => $user]);
        }
    }

    public function updateProfile() {
        if (Auth::check()) {
            $id = Auth::user()->id;
            $user = User::with('profile.address')->where('id', '=', $id)->first();
            // $address = Address::where('user_id','=', $id)->where('status','=','1')->first();
            return View::make('admin.updateprofile', ['user' => $user]);
        }
    }

    public function postProfile(Request $request, $id) {
        if ($id) {
            $profile = Profile::where('user_id', $id)->first();
            $data = $request->all();
            $validator = Validator::make($data, [
                        'email' => 'required|unique:user_info,email,' . $profile->id
            ]);

            if ($validator->fails()) {
                return Redirect::back()
                                ->withErrors($validator) // send back all errors to the login form
                                ->withInput();
            }

            
            $activeAddress = Address::where('user_id', $id)->where('status', '=', '1')->first();
            $newAddFlag = false;
            $oldData = [
                'user_info' => $profile->toArray(),
                'address' => (!empty($address)) ? $address->toArray() : []
            ];
            $oldData = json_encode($oldData);
            $profile->first_name = $data['first_name'];
            $profile->middle_name = $data['middle_name'];
            $profile->last_name = $data['last_name'];
            $profile->suffix = $data['suffix'];
            $profile->email = $data['email'];
            $profile->site_comm_pref = isset($data['site_comm_pref']) ? $data['site_comm_pref'] : '';
            $profile->voice = $data['voice'];
            $profile->update();
            if (isset($activeAddress) && $activeAddress->count()) {
                if ($activeAddress->address1 != $data['address1'] || $activeAddress->address2 != $data['address2'] || $activeAddress->city != $data['city'] || $activeAddress->state != $data['state'] || $activeAddress->country != $data['country'] || $activeAddress->zip_code != $data['zip_code']) {
                    $newAddFlag = true;
                }
            } else {
                $newAddFlag = true;
            }
            if ($newAddFlag) {
                Address::where('user_id', '=', $profile->id)->update(['status' => '0']);

                $address = new Address();
                $address->user_id = $id;
                $address->cid = $profile->cid;
                $address->address1 = $data['address1'];
                $address->address2 = $data['address2'];
                $address->city = $data['city'];
                $address->state = $data['state'];
                $address->country = $data['country'];
                $address->zip_code = $data['zip_code'];
                $address->status = 1;
                $address->save();

                $profile->address_id = $address->id;
                $profile->update();
            }


            //activity log
            \LogActivity::addToLog('ModProfile', Auth::check() ? Auth::user()->id : null, Auth::check() ? Auth::user()->cid : null, $profile->id, $profile->cid, $oldData);
            session()->flash('success', 'Profile updated successfully.');
            return redirect('/profile');
        }
    }

    public function getChangePassword() {
        return View::make('admin.change_password');
    }

    public function postChangePassword(Request $request) {
        if (Auth::check()) {
            $id = Auth::user()->id;
            $user = User::where('id', '=', $id)->first();
            // $data = $request->all();
            // echo "<pre>"; print_r($data); exit;
            $validator = Validator::make($request->all(), [
                        'current_password' => 'required',
                        'new_password' => 'required',
                        'confirm_password' => 'required|same:new_password'
            ]);
            if ($validator->fails()) {
                return Redirect::back()
                                ->withErrors($validator) // send back all errors to the login form
                                ->withInput();
            }

            $newPassword = Hash::make($request->get('new_password'));
            $user->password = $newPassword;
            $user->save();

            //logactivity
            \LogActivity::addToLog('ModPwd', Auth::check() ? Auth::user()->id : null, Auth::check() ? Auth::user()->cid : null);

            //Logout
            \LogActivity::addToLog('Logout', Auth::check() ? Auth::user()->id : null, Auth::check() ? Auth::user()->cid : null);
            Auth::logout();
            session()->forget('login_time');
            return redirect('/login');
        }
    }

    public function checkCurrentPassword(Request $request) {
        $data = $request->all();
        $id = Auth::user()->id;
        $user = User::where('id', '=', $id)->first();
        if (!Hash::check($data['password'], $user->password)) {
            echo false;
            exit;
        } else {
            echo true;
            exit;
        }
    }

}
