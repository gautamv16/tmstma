<?php

namespace App\Helpers;
use Request;
use App\Model\LogActivity as LogActivityModel;
use Auth;

class LogActivity
{

    public static function addToLog($subject,$id=null, $cID = null, $recordID=null, $recordCID='', $oldData='')
    {
    	$log = [];
    	$log['action_type'] = $subject;
        $log['user_id'] = $id;
    	$log['record_id'] = $recordID; 
    	$log['cid'] = $cID;
    	$log['record_cid'] = $recordCID;    	
        $log['old_data'] = $oldData;
        $log['ip'] = session('ip');
    	$log['location'] = session('city') . ", " . session('region') . ", " . session('country');
    	 LogActivityModel::create($log);
    }

    public static function logActivityLists()
    {
    	return LogActivityModel::with('user.profile')->latest()->get();
    }

}