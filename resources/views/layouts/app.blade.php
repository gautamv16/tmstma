<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Admin Dashboard</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ URL::asset('/css/style.css') }}" rel="stylesheet">

     <script src="{{ URL::asset('/assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/tether.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ URL::asset('/js/waves.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ URL::asset('/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ URL::asset('/js/custom.js') }}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/')}}">
                        Welcome To TMA-TMSAP
                    </a>
                </div>
                @if(Auth::check())
                <div class="navbar-collapse ">
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <li class="nav-item"> 
                            <a class="nav-link nav-toggler hidden-lg-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <?php 
                                $logintime = session('login_time');
                                $loginDate = session('login_date');
                                $now = new DateTime("now");
                                $before = new DateTime($logintime);
                                $interval = $now->diff($before);
                                $interval = $interval->format('%h:%i:%s'); 
                                $timepart = explode(":",$interval);
                                
                        ?>
                        <li class="nav-item hidden-md-down"><a>Login Time: {{ $logintime }} EST</a></li> 
                        <li class="nav-item hidden-md-down"><a>Date: {{ $loginDate }}</a></li> 
                        <li class="nav-item hidden-md-down"><a>Session Time: <span id="hours">{{$timepart[0]}}</span>:<span id="minutes">{{$timepart[1]}}</span>:<span id="seconds">{{$timepart[2]}}</span></a></li> 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">{{ Auth::user()->login_id }}<i class="mdi mdi-account-box"></i></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up show">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><i class="fa fa-user fa-2x"></i></div>
                                            <div class="u-text">
                                                <h4>{{ (isset(Auth::user()->profile->first_name) &&  Auth::user()->profile->first_name !='')  ? Auth::user()->profile->first_name . ' ' . Auth::user()->profile->last_name : Auth::user()->login_id }}</h4>
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{url('profile')}}"><i class="ti-user"></i> My Profile</a></li>
                                    <li><a href="{{ url('changepassword/')}}"><i class="ti-wallet"></i> Change Password</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{ url('/logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                @endif
            </nav>
        </header>
        @if(Auth::check())
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="waves-effect waves-dark active" href="{{ url('dashboard')}}"><i class="mdi mdi-gauge"></i>Dashboard</a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#"><i class="mdi mdi-buffer"></i>BA Requests</a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('users/')}}"><i class="mdi mdi-account-multiple"></i>Users</a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#"><i class="mdi mdi-emoticon"></i>Entities</a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#"><i class="mdi mdi-earth"></i>Tours</a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#"><i class="mdi mdi-book-open-variant"></i>Gig List</a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('activity-logs/')}}"><i class="mdi mdi-book-open"></i>Logs</a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#"><i class="mdi mdi-chart-bar"></i>Reports</a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        @endif
      <!-- content -->
      <div class="page-wrapper">
        @yield('content')
      </div>
      <!-- content ends here -->




        <footer class="footer">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li><a href="#">Advertising</a></li>
                        <li><a href="#">Business</a></li>
                    </ul>    
                </div>
                <div class="col-md-6">
                     <ul class="ul-right">
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Terms</a></li>
                        <li><a href="#">Settings</a></li>
                    </ul>    
                </div>
            </div>  
        </footer>

    </div>
    
    <script>
    $.get("http://ipinfo.io", function(response) {
      $('#ip').val(response.ip);
      $('#city').val(response.city);
      $('#region').val(response.region);
      $('#country').val(response.country);
      var ip = "{{session('ip')}}";
      if(ip == '' || ip == null || (ip != response.ip)){
        $.ajax({
            type:'POST',
            url:'/save-location',
            data:{_token :'<?php echo csrf_token() ?>',ip:response.ip,city:response.city,country:response.country,region:response.region},
            success:function(data){
            }
        });
      }
  }, "jsonp");
</script>
    <script>
        $(document).ready(function(){
           /* var logintime = new Date("<?php echo $logintime ?>");
            var now = new Date();
            console.log(now); */
            d = new Date();
           d.setHours(document.getElementById("hours").innerHTML);
           d.setMinutes(document.getElementById("minutes").innerHTML);
           d.setSeconds(document.getElementById("seconds").innerHTML, 0);
            setInterval(function () {
            document.getElementById("hours").innerHTML = d.getHours();
            document.getElementById("minutes").innerHTML = d.getMinutes();
            document.getElementById("seconds").innerHTML = d.getSeconds();
            d.setTime(d.getTime() + 1000);
            }, 1000);
        })

    </script>

</div>
</body>
</html>
