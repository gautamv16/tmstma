<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Admin Dashboard</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ URL::asset('/css/style.css') }}" rel="stylesheet">

     <script src="{{ URL::asset('/assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/tether.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ URL::asset('/js/waves.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ URL::asset('/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ URL::asset('/js/custom.js') }}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/')}}">
                        Welcome To TMA-TMSAP
                    </a>
                </div>                
            </nav>
        </header>
      <!-- content -->
      <div class="page-wrapper">
        @yield('content')
      </div>
      <!-- content ends here -->

        <footer class="footer">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li><a href="#">Advertising</a></li>
                        <li><a href="#">Business</a></li>
                    </ul>    
                </div>
                <div class="col-md-6">
                     <ul class="ul-right">
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Terms</a></li>
                        <li><a href="#">Settings</a></li>
                    </ul>    
                </div>
            </div>  
        </footer>

    </div>
</div>
<script>
    $.get("http://ipinfo.io", function(response) {
      var ip = "{{session('ip')}}";
      if(ip == '' || ip == null || (ip != response.ip)){
        $.ajax({
            type:'POST',
            url:'/save-location',
            data:{_token :'<?php echo csrf_token() ?>',ip:response.ip,city:response.city,country:response.country,region:response.region},
            success:function(data){
            }
        });
      }
  }, "jsonp");
</script>


</body>
</html>
