<ul class="Userlist">
    <li ><a class="{{ (isset($id) && $id == 'new') ? 'active' : ''}}" href="{{ url('user/new')}}" >New User</a></li>
    @if(isset($users) && !empty($users))
    @foreach($users as $user)
    @if(isset($user->profile) && empty($user->profile->reg_comp_date))
    <li data-status{{$user->id}}= "{{$user->status}}" class="user-list" id='{{ $user->id}}'><a class="{{ (isset($id) && $id == $user->id) ? 'active' : ''}}" href="javascript:void(0)">({{$user->profile->first_name . ' ' . $user->profile->last_name}})</a></li>
    @elseif(isset($user->profile->role) && $user->profile->role->code == 'client')
    <li data-statu{{$user->id}}s= "{{$user->status}}" class="user-list" id='{{ $user->id}}'><a class="{{ (isset($id) && $id == $user->id) ? 'active' : ''}}" href="javascript:void(0)"><b>{{$user->profile->first_name . ' ' . $user->profile->last_name}}</b></a></li>
    @else
    <li data-status{{$user->id}}= "{{$user->status}}" class="user-list" id='{{ $user->id}}'><a class="{{ (isset($id) && $id == $user->id) ? 'active' : ''}}" href="javascript:void(0)">{{$user->profile->first_name . ' ' . $user->profile->last_name}}</a></li>
    @endif


    @endforeach

    @endif
</ul>

<!-- Modal -->
<div id="regInProModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Forbidden!</h4>
            </div>
            <div class="modal-body">
                <div class="forbidden-img"><img src="{{ url('/images/forbidden.jpg')}}"/></div>
                <div>This User is under registration process. You will be able to edit the record once registration process is complete.<br/>
                       Thanks!
                </div>
            </div>

        </div>

    </div>
</div>

<script>
    function viewUserEditMode(id) {

        window.location.href = "{{ url('user/view-edit/')}}/" + id;
    }

    function viewUserUpdateMode(id) {
        var uStatus = $('#' + id).attr('data-status' + id);
        if (uStatus == 0) {
             $('#regInProModal').modal('show');
             return false;
        } else {
            window.location.href = "{{ url('user/update/')}}/" + id;
        }
    }

    var DELAY = 200, clicks = 0, timer = null;

    $(function () {

        $(".user-list").on("click", function (e) {
            clicks++;  //count clicks
            if (clicks === 1) {
                var id = $(this).attr('id');

                timer = setTimeout(function (e) {
                    clicks = 0;
                    viewUserEditMode(id);  //perform single-click action    
                    //after action performed, reset counter

                }, DELAY);

            } else {

                clearTimeout(timer);    //prevent single-click action
                var id = $(this).attr('id');
                clicks = 0;
                viewUserUpdateMode(id);  //perform double-click action
                //after action performed, reset counter
            }

        })
                .on("dblclick", function (e) {
                    e.preventDefault();  //cancel system double-click event
                });

    });

</script>