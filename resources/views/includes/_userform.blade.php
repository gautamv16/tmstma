<form class="form-horizontal form-material" id="user-form" action="{{ url('send-request/')}}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-3 mr-10">
            <label>First Name</label>
            <input value="{{ old('first_name')}}" name="first_name" id="first_name" type="text" placeholder="" class="form-control form-control-line">
        </div>
        <div class="col-md-3 mr-10">
            <label>Middle Name</label>
            <input value="{{ old('middle_name')}}" name="middle_name" type="text" placeholder="" class="form-control form-control-line">
        </div>
        <div class="col-md-3 mr-10">
            <label>Last Name</label>
            <input value="{{ old('last_name')}}" name="last_name" id="last_name" type="text" placeholder="" class="form-control form-control-line">
        </div>
        <div class="col-md-3 mr-10">
            <label>Suffix</label>
            <input value="{{ old('suffix')}}" name="suffix" type="text" placeholder="" class="form-control form-control-line">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6  mr-10">
            <label>Email</label>
            <input value="{{ old('email')}}" name="email" id="email"type="email" placeholder="admin@gmail.com" class="form-control form-control-line">
            @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
        </div>
        <div class="col-md-6  mr-10">
            <label>Mobile</label>
            <input name="mobile" id="mobile" type="text" placeholder="" class="form-control form-control-line">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 mr-10">
            <label>User Type</label>
            <select onchange="onUserTypeChange(this)" name="user_type" id="user_type" class="form-control form-control-line">
                @foreach($types as $type)
                <option value="{{$type->id}}">{{ $type->user_type}}</option>
                @endforeach
            </select>

        </div>
        <div class="col-md-4 mr-10">
            <label>CID</label>
            <input value="{{ old('cid')}}" readonly="readonly" name="cid" id="cid" type="text" placeholder="" class="form-control form-control-line">
            @if ($errors->has('cid')) <p class="help-block">{{ $errors->first('cid') }}</p> @endif
        </div>
        <div class="col-md-4 mr-10">
            <label>User Name</label>
            <input value="{{ old('login_id')}}" readonly="readonly" name="login_id" id="login_id" type="text" placeholder="" class="form-control form-control-line">
            @if ($errors->has('login_id')) <p class="help-block">{{ $errors->first('login_id') }}</p> @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6  mr-10">
            <label>Address1</label>
            <input value="{{ old('address1')}}" name="address1" type="text" placeholder="" class="form-control form-control-line">
        </div>
        <div class="col-md-6  mr-10">
            <label>Address2</label>
            <input value="{{ old('address2')}}" name="address2" type="text" placeholder="" class="form-control form-control-line">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 mr-10">
            <label>City</label>
            <input value="{{ old('city')}}" name="city" type="text" placeholder="" class="form-control form-control-line">

        </div>
        <div class="col-md-3 mr-10">
            <label>State/Province</label>
            <select class="form-control form-control-line" name="state">
                <option value="New York">New York</option>
            </select>
        </div>
        <div class="col-md-3 mr-10">
            <label>Country</label>
            <select class="form-control form-control-line" name="country">
                <option class="United States">United States</option>
            </select>
        </div>
        <div class="col-md-3 mr-10">
            <label>Zip/Postal Code</label>
            <input value="{{ old('zip_code')}}" name="zip_code" type="text" placeholder="" class="form-control form-control-line">
        </div>
    </div>
    <div class="row" id="instrument-block" style="display: none;">
        <div class="col-md-6 mr-10">
            <div>
                <label>Instrument</label>
                <input value="{{ old('instrument')}}" id="instrument" disabled="disabled" name="instrument" type="text" placeholder="" class="form-control form-control-line">
            </div>
            <div class="mr-10">
                <label class="mr-8">Backline / Instrument Requirements</label>
                <textarea value="{{ old('backline')}}" id="backline" disabled="disabled" name="intrument_requirement" class="form-control form-control-line" rows="3"></textarea>
            </div>
        </div>
        <div class="col-md-6 mr-10">
            <label>Client Associations</label>
            <textarea value="{{ old('client_association')}}" name="client_association" class="form-control form-control-line" rows="7"></textarea>
        </div>
    </div>
    <div class="form-group text-center mr-10">
        <div>
            <button class="btn btn-success" {{ (isset($editflag) && !$editflag) ? 'disabled' : '' }} id="send">{{ (isset($btn_label) && $btn_label !='') ? $btn_label : "Send"}}</button>
            @if(isset($cancelflag) && !$cancelflag)
            <button class="btn btn-danger" disabled>Cancel</button>
            @else
            <a href="{{ url('users/')}}" class="btn btn-danger">Cancel</a>
            @endif
        </div>
    </div>
</form>