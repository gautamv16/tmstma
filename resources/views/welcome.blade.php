<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Admin Dashboard</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ URL::asset('/css/style.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('/js/jquery.min.js') }}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
<div class="main-wrapper">
<div class="page-wrapper" style="min-height:100vh;">
    <div class="container-fluid">
        <div class="row flex_pnls">
        <div class="col-lg-2 col-xlg-2 col-md-2">
            <img src="images/logo.jpg">
        </div>
            <div class="col-lg-4 col-xlg-4 col-md-4 offset-md-1">
                <h2 class="form-title login-title">Tour Management System Access Portal</h2>
               @if(Session::has('error'))
                    <div class="alert alert-danger">
                    {{ Session::get('error') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-block">
                        <form class="form-horizontal form-material" action="{{ url('/post-login')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Login ID</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="xyz" class="form-control form-control-line" name="login_id" id="login-id">
                                    @if ($errors->has('login_id')) <p class="help-block">{{ $errors->first('login_id') }}</p> @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Password</label>
                                <div class="col-md-12">
                                    <input type="password" value="password" name="password" class="form-control form-control-line">
                                    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                                </div>
                            </div>
                            <input type="hidden" name="ip" id="ip" value="">
                            <input type="hidden" name="city" id="city" value="">
                            <input type="hidden" name="region" id="region" value="">
                            <input type="hidden" name="country" id="country" value="">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success" type="submit">Log In</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
<script src="{{ URL::asset('/assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ URL::asset('/js/waves.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ URL::asset('/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script>
  $.get("http://ipinfo.io", function(response) {
    console.log(response);
    $('#ip').val(response.ip);
    $('#city').val(response.city);
    $('#region').val(response.region);
    $('#country').val(response.country);
    /*$.ajax({
        type:'POST',
        url:'/save-location',
        data:{_token :'<?php echo csrf_token() ?>',ip:response.ip,city:response.city,country:response.country,region:response.region},
        success:function(data){
           console.log(data);
        }
    });*/



}, "jsonp"); 


</script>
</body>
</html>