@extends('layouts.app')
@section('content')
<?php 
if(isset($user->profile->address)){
 $address = $user->profile->address;
}
?>
 <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-xlg-10 col-md-10 offset-md-1">
            <h2 class="form-title">View Profile</h2>
             @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ Session::get('success') }}
                </div>
                @endif
            <div class="card">
                <div class="card-block">
                        <form class="form-horizontal form-material">
                            <div class="row">
                                <div class="col-md-4 mr-10">
                                    <label>CID</label>
                                    <input disabled type="text" placeholder="CID No." value="{{ $user->cid}}" class="form-control form-control-line">
                                </div>
                                <div class="col-md-4 mr-10">
                                    <label>User Name</label>
                                    <input disabled type="text" placeholder="username" value="{{ $user->login_id}}" class="form-control form-control-line">
                                </div>
                                <div class="col-md-4 mr-10">
                                    <label>User Type</label>
                                    <input disabled type="text" placeholder="User Type" value="{{ ((!is_int($user->profile->user_type)) && $user->profile->user_type == 'Admin') ? $user->profile->user_type :  $user->profile->role->user_type}}" class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 mr-10">
                                    <label>First Name</label>
                                    <input disabled type="text" placeholder="" value="{{ $user->profile->first_name}}" class="form-control form-control-line">
                                </div>
                                <div class="col-md-3 mr-10">
                                    <label>Middle Name</label>
                                    <input disabled type="text" placeholder="" value="{{ $user->profile->middle_name}}" class="form-control form-control-line">
                                </div>
                                <div class="col-md-3 mr-10">
                                    <label>Last Name</label>
                                    <input disabled type="text" placeholder="" value="{{ $user->profile->last_name}}" class="form-control form-control-line">
                                </div>
                                <div class="col-md-3 mr-10">
                                    <label>Suffix</label>
                                    <input disabled type="text" placeholder="" value="{{ $user->profile->suffix}}" class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 mr-10">
                                    <label>Zip/Postal Code</label>
                                    <input disabled type="text" value="{{ isset($address->zip_code) ? $address->zip_code : ''}}" name="zip_code" placeholder="" class="form-control form-control-line">
                                </div>
                                <div class="col-md-3 mr-10">
                                    <label>City</label>
                                    <input disabled type="text" name="city" value="{{ isset($address->city) ? $address->city : ''}}" placeholder="" class="form-control form-control-line">
                                </div>
                                <div class="col-md-3 mr-10">
                                    <label>State/Province</label>
                                    <select disabled name="state" class="form-control form-control-line">
                                        <option value="New York">New York</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mr-10">
                                    <label>Country</label>
                                    <select disabled class="form-control form-control-line" name="country">
                                        <option class="United States">United States</option>
                                    </select>
                                </div>
                                
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6  mr-10">
                                    <label>Address1</label>
                                    <input disabled type="text" placeholder="" value="{{ isset($address->address1) ? $address->address1 : ''  }}" class="form-control form-control-line">
                                </div>
                                <div class="col-md-6  mr-10">
                                    <label>Address2</label>
                                    <input disabled type="text" placeholder="" value="{{ isset($address->address2) ? $address->address2 : ''}}" class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6  mr-10">
                                    <label>Email</label>
                                    <input disabled type="email" placeholder="admin@gmail.com" value="{{ $user->profile->email}}" class="form-control form-control-line">
                                </div>
                                <div class="col-md-6  mr-10">
                                    <label>Voice</label>
                                    <input disabled type="text" placeholder="" value="{{ $user->profile->voice}}" class="form-control form-control-line">
                                </div>
                            </div>
                            
                            <div class="row mr-10">
                                <div class="mr-10">
                                    <span>Allow this site to contact me by:</span>&nbsp;
                                    <div class="form-check form-check-inline">
                                        <input disabled type="radio" {{ (isset($user->profile->site_comm_pref) && $user->profile->site_comm_pref == 'Email') ? "checked" : "" }} name="site_comm_pref" class="form-check-input" id="materialInline1" value="Email">
                                        <label disabled class="form-check-label" for="materialInline1">Email</label>
                                    </div> 
                                    <div class="form-check form-check-inline">
                                        <input disabled type="radio" name="site_comm_pref" {{ (isset($user->profile->site_comm_pref) &&  $user->profile->site_comm_pref == 'Text') ? "checked" : "" }} class="form-check-input" id="materialInline2" value="Text">
                                        <label class="form-check-label" for="materialInline2">Text</label>
                                    </div> 
                                    <div class="form-check form-check-inline">
                                        <input disabled type="radio" name="site_comm_pref" {{ (isset($user->profile->site_comm_pref) && $user->profile->site_comm_pref == 'Both') ? "checked" : "" }} class="form-check-input" id="materialInline3" value="Both">
                                        <label class="form-check-label" for="materialInline3">Both</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="form-group text-center mr-10">
                                <div>
                                    <a href="{{url('updateprofile')}}" class="btn btn-warning" >Edit</a>
                                    <button type="button" class="btn btn-danger" disabled>Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        @endsection