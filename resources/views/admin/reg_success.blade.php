@extends('layouts.guest_app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-xlg-6 col-md-6 offset-md-3" style="margin-top: 40px;">
            <h2> Registration Complete! </h2>
            <div class="card">
                <div class="card-block">
                   Welcome to TMA-TMSAP. you will be shortly redirected to login page. <br/>
                   Login & Enjoy!
                </div>
            </div>
        </div>
    </div>
</div>
 <script>
   $(document).ready(function(){
       window.setTimeout(function(){
        // Move to a new location or you can do something else
        window.location.href = "{{ url('login/')}}";

    }, 2000);
   })
</script>


@endsection