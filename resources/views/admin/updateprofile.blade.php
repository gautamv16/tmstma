@extends('layouts.app')
@section('content')
<?php
$address = $user->profile->address;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10 col-xlg-10 col-md-10 offset-md-1">
            <h2 class="form-title">Update Profile</h2>
            <div class="card">
                <div class="card-block">
                    <form class="form-horizontal form-material" action="{{ url('updateprofile/' . $user->id)}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-4 mr-10">
                                <label>CID</label>
                                <input  readonly type="text" placeholder="CID No." name="cid" value="{{ $user->cid}}" class="form-control form-control-line">
                            </div>
                            <div class="col-md-4 mr-10">
                                <label>User Name</label>
                                <input  readonly type="text" placeholder="username" name="login_id" value="{{ $user->login_id}}" class="form-control form-control-line">
                            </div>
                            <div class="col-md-4 mr-10">
                                <label>User Type</label>
                                <input  readonly type="text" placeholder="User Type" name="user_type" value="{{ ((!is_int($user->profile->user_type)) && $user->profile->user_type == 'Admin') ? $user->profile->user_type :  $user->profile->role->user_type}}" class="form-control form-control-line">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 mr-10">
                                <label>First Name</label>
                                <input  type="text" placeholder="" name="first_name" value="{{ $user->profile->first_name}}" class="form-control form-control-line">
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Middle Name</label>
                                <input  type="text" placeholder="" name="middle_name" value="{{ $user->profile->middle_name}}" class="form-control form-control-line">
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Last Name</label>
                                <input  type="text" placeholder=""  name="last_name" value="{{ $user->profile->last_name}}" class="form-control form-control-line">
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Suffix</label>
                                <input  type="text" placeholder="" name="suffix" value="{{ $user->profile->suffix}}" class="form-control form-control-line">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 mr-10">
                                <label>Zip/Postal Code</label>
                                <input  type="text" name="zip_code" value="{{ isset($address->zip_code) ? $address->zip_code : ''}}" placeholder="" class="form-control form-control-line">
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>City</label>
                                <input  type="text" name="city" value="{{ isset($address->city) ? $address->city : ''}}" placeholder="" class="form-control form-control-line">
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>State/Province</label>
                                <select  name="state" class="form-control form-control-line">
                                    <option {{ (isset($address->state) && $address->state == 'New York') ? 'selected' : '' }}>New York</option>
                                </select>
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Country</label>
                                <select  class="form-control form-control-line" name="country">
                                    <option {{ (isset($address->country) && $address->country == 'United States') ? 'selected' : '' }}>United States</option>
                                </select>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6  mr-10">
                                <label>Address1</label>
                                <input  type="text" placeholder="" name="address1" value="{{ isset($address->address1) ? $address->address1 : ''}}" class="form-control form-control-line">
                            </div>
                            <div class="col-md-6  mr-10">
                                <label>Address2</label>
                                <input  type="text" placeholder="" name="address2" value="{{ isset($address->address2) ? $address->address2 : ''}}"class="form-control form-control-line">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6  mr-10">
                                <label>Email</label>
                                <input  type="email" placeholder="admin@gmail.com" name="email"  value="{{ $user->profile->email}}" class="form-control form-control-line">
                                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                            </div>
                            <div class="col-md-6  mr-10">
                                <label>Voice</label>
                                <input  type="text" placeholder="" name="voice" value="{{ $user->profile->voice }}" class="form-control form-control-line">
                            </div>
                        </div>

                        <div class="row mr-10">
                            <div class="mr-10">
                                <span>Allow this site to contact me by:</span>&nbsp;
                                <div class="form-check form-check-inline">
                                    <input  type="radio" name="site_comm_pref" {{ (isset($user->profile->site_comm_pref) && $user->profile->site_comm_pref == 'Email') ? "checked" : "" }} class="form-check-input" id="materialInline1" value="Email">
                                    <label  class="form-check-label" for="materialInline1">Email</label>
                                </div> 
                                <div class="form-check form-check-inline">
                                    <input  type="radio" name="site_comm_pref" {{ (isset($user->profile->site_comm_pref) && $user->profile->site_comm_pref == 'Text') ? "checked" : "" }} class="form-check-input" id="materialInline2" value="Text">
                                    <label class="form-check-label" for="materialInline2">Text</label>
                                </div> 
                                <div class="form-check form-check-inline">
                                    <input  type="radio" name="site_comm_pref" {{ (isset($user->profile->site_comm_pref) && $user->profile->site_comm_pref == 'Both') ? "checked" : "" }} class="form-check-input" id="materialInline3" value="Both">
                                    <label class="form-check-label" for="materialInline3">Both</label>
                                </div> 
                            </div>
                        </div>
                        <div class="form-group text-center mr-10">
                            <div>
                                <button class="btn btn-success" type="submit">Update</button>
                                <a href="{{ url('profile')}}" class="btn btn-danger" >Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function updateProfile() {
        $('.view-edit-profile').hide();
        $('.update-edit-profile').show();
    }

    function cancelUpdateProfile() {
        $('.view-edit-profile').show();
        $('.update-edit-profile').hide();
    }
</script>


@endsection