@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible col-sm-12">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> {{ Session::get('success') }}
        </div>
        @endif

        <div class="col-lg-3 col-xlg-3 col-md-3">
            <h2 class="form-title">User List</h2>
            <div class="card">
                <div class="card-block">
                    @include('includes.user_nav',['users'=>$users,'id'=>$userInfo->id])

                </div>
            </div> 
            <!--            <div class="text-center mr-10">
                            <div>
                                <button class="btn btn-warning">Edit</button>
                                <button class="btn btn-danger" disabled>Cancel</button>
                            </div>
                        </div>  -->
        </div>
        <div class="col-lg-9 col-xlg-9 col-md-9">
            <h2 class="form-title">Personal Details</h2>
            <div class="card">
                <div class="card-block">
                    <form class="form-horizontal form-material" action="{{ url('user/update/' . $userInfo->id)}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-3 mr-10">
                                <label>First Name</label>
                                <input value="{{$userInfo->profile->first_name}}"   name="first_name" id="first_name" type="text" placeholder="" class="form-control form-control-line">
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Middle Name</label>
                                <input value="{{$userInfo->profile->middle_name}}"   name="middle_name" type="text" placeholder="" class="form-control form-control-line">
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Last Name</label>
                                <input value="{{$userInfo->profile->last_name}}"   name="last_name" id="last_name" type="text" placeholder="" class="form-control form-control-line">
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Suffix</label>
                                <input value="{{$userInfo->profile->suffix}}"   name="suffix" type="text" placeholder="" class="form-control form-control-line">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6  mr-10">
                                <label>Email</label>
                                <input value="{{$userInfo->profile->email}}"   name="email" id="email"type="email" placeholder="admin@gmail.com" class="form-control form-control-line">
                                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                            </div>
                            <div class="col-md-6  mr-10">
                                <label>Mobile</label>
                                <input value="{{$userInfo->profile->mobile}}"   name="mobile" id="mobile" type="text" placeholder="" class="form-control form-control-line">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mr-10">
                                <label>User Type</label>
                                <select    onchange="onUserTypeChange(this)" name="user_type" id="user_type" class="form-control form-control-line">
                                    @foreach($types as $type)
                                    <option {{ ($userInfo->profile->user_type == $type->id) ? 'selected' : ''}} value="{{$type->id}}">{{ $type->user_type}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="col-md-4 mr-10">
                                <label>CID</label>
                                <input value="{{$userInfo->cid}}"   readonly="readonly" name="cid" id="cid" type="text" placeholder="" class="form-control form-control-line">
                                @if ($errors->has('cid')) <p class="help-block">{{ $errors->first('cid') }}</p> @endif
                            </div>
                            <div class="col-md-4 mr-10">
                                <label>User Name</label>
                                <input value="{{$userInfo->login_id}}"   readonly="readonly" name="login_id" id="login_id" type="text" placeholder="" class="form-control form-control-line">
                                @if ($errors->has('login_id')) <p class="help-block">{{ $errors->first('login_id') }}</p> @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6  mr-10">
                                <label>Address1</label>
                                <input value="{{$userInfo->address->address1}}"   name="address1" type="text" placeholder="" class="form-control form-control-line">
                            </div>
                            <div class="col-md-6  mr-10">
                                <label>Address2</label>
                                <input value="{{$userInfo->address->address2}}"   name="address2" type="text" placeholder="" class="form-control form-control-line">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 mr-10">
                                <label>City</label>
                                <input value="{{$userInfo->address->city}}"   name="city" type="text" placeholder="" class="form-control form-control-line">

                            </div>
                            <div class="col-md-3 mr-10">
                                <label>State/Province</label>
                                <select   class="form-control form-control-line" name="state">
                                    <option value="New York">New York</option>
                                </select>
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Country</label>
                                <select   class="form-control form-control-line" name="country">
                                    <option class="United States">United States</option>
                                </select>
                            </div>
                            <div class="col-md-3 mr-10">
                                <label>Zip/Postal Code</label>
                                <input value="{{$userInfo->address->zip_code}}"   name="zip_code" type="text" placeholder="" class="form-control form-control-line">
                            </div>
                        </div>
                        <div id="instrument-block" class="row" style="display: {{ (isset($userInfo->profile->role) && ($userInfo->profile->role->code == 'client' || $userInfo->profile->role->code == 'musician' )) ? 'block' : 'none'}}">
                            <div class="col-md-6 mr-10">
                                <div>
                                    <label>Instrument</label>
                                    <input value="{{$userInfo->profile->instrument}}"   id="instrument"   name="instrument" type="text" placeholder="" class="form-control form-control-line">
                                </div>
                                <div class="mr-10">
                                    <label class="mr-8">Backline / Instrument Requirements</label>
                                    <textarea value="{{$userInfo->profile->backline}}"   id="backline"   name="intrument_requirement" class="form-control form-control-line" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6 mr-10">
                                <label>Client Associations</label>
                                <textarea value="{{$userInfo->profile->client_association}}"   name="client_association" class="form-control form-control-line" rows="7"></textarea>
                            </div>
                        </div>
                        <div class="form-group text-center mr-10">
                            <div>

                                @if($userInfo->status == 1 || $userInfo->status == 2)
                                <button  class="btn btn-success" id="send" type="submit">update</button>
                                @else
                                <a href="javascript:void(0)" onclick="regInProcessPrompt()" class="btn btn-success" id="send">Update</a>
                                @endif


                                <a href="{{url('users/')}}" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

<script>

    function regInProcessPrompt() {
        $('#regInProModal').modal('show');
    }
</script>

<script>

    function validateForm() {
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var email = $('#email').val();
        var usertype = $('#user_type').val();
         var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if ((first_name != '' && last_name != '' && email != '' && usertype != '') && pattern.test(email)) {
            $("#send").removeAttr("disabled");
        } else {
            $("#send").prop("disabled", true);
            if (!pattern.test(email) && email != '')
            {
                alert('Please enter valid email id.');
            }
        }
    }

    function onUserTypeChange(e) {
        var val = $(e).val();
        var txt = $(e).find("option:selected").text();

        if (txt == 'Client/Band Leader' || txt == 'Musician') {
            $('#instrument-block').show();
            $('#instrument').val('');
            $('#backline').val('');
            $('#instrument').prop('disabled', false);
            $('#backline').prop('disabled', false);
        } else {
            $('#instrument-block').hide();
            $('#instrument').val('');
            $('#backline').val('');
            $('#instrument').prop('disabled', true);
            $('#backline').prop('disabled', true);
        }
    }
    $('#first_name, #last_name, #email, #user_type').on('change', validateForm);






</script>



@endsection