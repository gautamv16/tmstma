@extends('layouts.guest_app')
@section('content')
 <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-xlg-6 col-md-6 offset-md-3" style="text-align: center; margin-top: 100px;">
               <img style="width:115px;" src="{{ url('/images/warning.png')}}" />
            
            <div class="card">
                <div class="card-block">
                      <h4> @if(Session::has('message'))
                            {{ Session::get('message') }}
                            @else
                            Oops! something went wrong.                                            
                        @endif
                      </h4>
                    </div>
                </div>
            </div>
        </div>
     
        @endsection