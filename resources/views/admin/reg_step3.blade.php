@extends('layouts.guest_app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-xlg-6 col-md-6 offset-md-3" style="margin-top: 40px;">
            <h2> Create Password </h2>
            <div class="card">
                <div class="card-block">
                     <form class="form-horizontal form-material" method="POST" action="{{ url('aun-step3/')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_id" value="{{ $user->id }}" >
                        <div class="row">
                            <div class="col-md-12 mr-10 inpt-checkr">
                                <label for="example-email">Password</label>
                                <input  name="password" type="password" placeholder="" class="form-control form-control-line">
                                 @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                            </div>                            
                        </div>
                        
                         <div class="row">
                            <div class="col-md-12 mr-10 inpt-checkr">
                                <label for="example-email">Confirm Password</label>
                                <input name="confirm_password" type="password" placeholder="" class="form-control form-control-line">
                                 @if ($errors->has('confirm_password')) <p class="help-block">{{ $errors->first('confirm_password') }}</p> @endif
                            </div>                            
                        </div>
                        <div class="form-group mr-10">
                            <div>
                                <button id="update-btn" class="btn btn-success" type="submit">Send</button>
                            </div>
                        </div>
                     </form>
                </div>
            </div>
        </div>
    </div>
</div>
 <script>
    function checkCode(e){
        var val = $(e).val();
        if(val != ''){
           $('#update-btn').prop('disabled', false);
          }else{
               $('#update-btn').prop('disabled', true);
          }  
          return;
    }
</script>


@endsection