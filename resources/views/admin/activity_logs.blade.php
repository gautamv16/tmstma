@extends('layouts.app')
@section('content')
 <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
            <h2 class="form-title">Site Activity Logs</h2>
             @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ Session::get('success') }}
                </div>
                @endif
            <div class="card">
                <div class="card-block">
                        <table class="table table-responsive table-striped cstm-table">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Date/Time</th>
                                    <th>IP</th>
                                    <th>CID</th>
                                    <th>Action</th>
                                    <th>old Record</th>
                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                                  @foreach($logs as $k=>$log)                                
                                    <tr>
                                       <td>{{$k+1}}</td>
                                      <td>{{ date('d-M-Y H:i:s',strtotime($log->created_at))}}</td>
                                      <td>{{$log->ip}}</td>
                                      <td>{{$log->cid}}</td>
                                       <td>{{$log->action_type}}</td> 
                                       <td>
                                           @if(isset($log->old_data) && $log->old_data !='')
                                           <div id="old_data_{{$log->id}}" class="data actvty-data">{{ $log->old_data}}</div>
                                           <a style="float:left" href="javascript:void(0);" onclick="viewAll('{{ $log->id}}')">View All</a>   
                                            @endif
                                        </td>
                                      
                                    </tr>

                                  @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="oldRecordModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Old Record</h4>
                </div>
                <div class="modal-body">
                    <div class="full-old-record">
                    </div>
                </div>
                
                </div>

            </div>
        </div>

        <script>
            function viewAll(id){
                var old_data = $('#old_data_'+id).html();
                $('.full-old-record').html('');
                $('.full-old-record').html(old_data);
                $('#oldRecordModal').modal('show');
            }
        </script>
        @endsection