@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible col-sm-12">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> {{ Session::get('success') }}
        </div>
        @endif

        <div class="col-lg-3 col-xlg-3 col-md-3">
            <h2 class="form-title">User List</h2>
            <div class="card">
                <div class="card-block">
                    @include('includes.user_nav',['users'=>$users,'id'=>'new'])                    
                </div>
            </div> 
        </div>
        <div class="col-lg-9 col-xlg-9 col-md-9">
            <h2 class="form-title">Personal Details</h2>
            <div class="card">
                <div class="card-block">                    
                    @include('includes._userform',['cancelflag'=>false,'editflag'=>false])       

                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        $('#user-form input, #user-form select, #user-form textarea, #user-form radio').attr('disabled', false);

    })
    function validateForm() {
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var email = $('#email').val();
        var usertype = $('#user_type').val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if ((first_name != '' && last_name != '' && email != '' && usertype != '') && pattern.test(email)) {
            $("#send").removeAttr("disabled");
        } else {
            $("#send").prop("disabled", true);
            if (!pattern.test(email) && email != '')
            {
                alert('Please enter valid email id.');
            }
        }
    }

    function createUserKeys() {
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        if (first_name != '' && last_name != '') {
            $.ajax({
                type: "GET",
                url: "{{ url('user-keys/')}}",
                data: {first_name: first_name, last_name: last_name},
                success: function (res) {
                    $('#cid').val(res.cid);
                    $('#login_id').val(res.username);
                }
            })
        }
    }

    function onUserTypeChange(e) {
        var val = $(e).val();
        var txt = $(e).find("option:selected").text();

        if (txt == 'Client/Band Leader' || txt == 'Musician') {
            $('#instrument-block').show();
            $('#instrument').val('');
            $('#backline').val('');
            $('#instrument').prop('disabled', false);
            $('#backline').prop('disabled', false);
        } else {
            $('#instrument-block').hide();
            $('#instrument').val('');
            $('#backline').val('');
            $('#instrument').prop('disabled', true);
            $('#backline').prop('disabled', true);
        }
    }
    $('#first_name, #last_name, #email, #user_type').on('change', validateForm);
    $('#first_name, #last_name').on('change', createUserKeys);
</script>

@endsection