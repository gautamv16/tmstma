@extends('layouts.app')
@section('content')
 <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-xlg-6 col-md-6 offset-md-3">
            <h2 class="form-title">Change Password</h2>
             @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ Session::get('success') }}
                </div>
                @endif
            <div class="card">
                <div class="card-block">
                    <form class="form-horizontal form-material" method="post" action="{{ url('changepassword/')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12 mr-10 inpt-checkr">
                                <label for="example-email">Current Password</label>
                                <input name="current_password" onblur="checkCurrentPass(this)" type="password" placeholder="" class="form-control form-control-line">
                                <span style="display:none;" class="loader chkr-signs" id="crnt-pass-loader"><i class="fa fa-spinner fa-spin fa-1x"></i></span>
                                <span style="display:none;" class="checker chkr-signs" id="crnt-pass-checker"></span>
                                 @if ($errors->has('current_password')) <p class="help-block">{{ $errors->first('current_password') }}</p> @endif
                            </div>
                            <div class="col-md-12 mr-10">
                                <label for="example-email">New Password</label>
                                <input name="new_password" id="new_password" type="password" placeholder="" class="form-control form-control-line">
                                 @if ($errors->has('new_password')) <p class="help-block">{{ $errors->first('new_password') }}</p> @endif
                            </div>
                            <div class="col-md-12 mr-10 inpt-checkr">
                                <label for="example-email">Re-enter Password</label>
                                <input name="confirm_password" id="confirm_password" onblur="confirmPassword()" type="password" placeholder="" class="form-control form-control-line">
                                <span style="display:none;" class="checker chkr-signs" id="cnfrm-pass-checker"></span>
                            @if ($errors->has('confirm_password')) <p class="help-block">{{ $errors->first('confirm_password') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group mr-10">
                            <div>
                                <button id="update-btn" class="btn btn-success" disabled="disables">Update</button>
                                <a href="{{ url('dashboard/')}}" class="btn btn-danger">cancel</a>
                            </div>
                        </div>
                     </form>
                    </div>
                </div>
            </div>
        </div>

<script>
var current_pass_ok = false;
var confirm_pass_ok = false;
    function checkCurrentPass(e){
       var password = $(e).val();
       $('#crnt-pass-checker').html("");
       $('#crnt-pass-loader').show();
       $.ajax({
          type:"get",
          url:"<?php echo url('current-passcheck'); ?>",
          data:{password:password},
          success:function(res){
                if(res){
                    current_pass_ok = true;
                    $('#crnt-pass-loader').hide();
                    $('#crnt-pass-checker').html('<span style="color:green" class="fa fa-check"></span>');
                    $('#crnt-pass-checker').show();
                }else{
                    current_pass_ok = false;
                    $('#crnt-pass-loader').hide();
                    $('#crnt-pass-checker').html('<span style="color:red;" class="fa fa-remove"></span>');
                    $('#crnt-pass-checker').show();
                }
          }
       });
       setBtnStatus();
    }

    function confirmPassword(){
        var newPass = $('#new_password').val();
        var confirmPass = $('#confirm_password').val();        
        if(newPass === confirmPass){
            $('#cnfrm-pass-checker').html('<span style="color:green" class="fa fa-check"></span>');          
           confirm_pass_ok = true;
        }else{
            $('#cnfrm-pass-checker').html('<span style="color:red;" class="fa fa-remove"></span>');
        }
        $('#cnfrm-pass-checker').show();
        setBtnStatus();
        
        
    }

    function setBtnStatus(){
        if(confirm_pass_ok && current_pass_ok){
            console.log('remove dis');
            $('#update-btn').prop('disabled', false);
        }else{
             $('#update-btn').prop('disabled', true);
        }
    }
</script>
        @endsection