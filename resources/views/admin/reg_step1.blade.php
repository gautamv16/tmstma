@extends('layouts.guest_app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-xlg-6 col-md-6 offset-md-3" style="margin-top: 40px;">
            <h2 class="form-title">Validate Username</h2>
            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('error') }}
            </div>
            @endif
            <div class="card">
                <div class="card-block">
                    <form class="form-horizontal form-material" method="post" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="uname" value="{{ $uname }}" >
                        <input type="hidden" name="ip" id="ip" value="">
                        <input type="hidden" name="city" id="city" value="">
                        <input type="hidden" name="region" id="region" value="">
                        <input type="hidden" name="country" id="country" value="">
                        <div class="row">
                            <div class="col-md-12 mr-10 inpt-checkr">
                                <label for="example-email">Username</label>
                                <input onchange="checkUsername(this)" name="username" type="text" placeholder="" class="form-control form-control-line">
                                @if ($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                            </div>                            
                        </div>
                        <div class="form-group mr-10">
                            <div>
                                <button id="update-btn" class="btn btn-success" disabled="disables" type="submit">Validate</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function checkUsername(e) {
            var val = $(e).val();
            if (val != '') {
                $('#update-btn').prop('disabled', false);
            } else {
                $('#update-btn').prop('disabled', true);
            }
            return;
        }
    </script>
    <script>
        $(document).ready(function () {
            $.get("http://ipinfo.io", function (response) {
                $('#ip').val(response.ip);
                $('#city').val(response.city);
                $('#region').val(response.region);
                $('#country').val(response.country);
            }, "jsonp");
        })
    </script>

    @endsection