<p>Hi,</p>
<p> User failed to complete New Registration steps!</p>
<p>User Details are: </p>
<p>CID: {{ $user->cid }}</p>
<p>Username/Login ID: {{ $user->login_id }}</p>
<p>User created at: {{ date('d-M-Y H:i:s', strtotime($user->created_at)) }}</p>
<p>Email : {{ $user->profile->email }}</p>
<p>Thanks,</p>
<p>TMS Team</p>