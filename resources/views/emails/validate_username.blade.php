<p>Hi {{ $data['first_name'] }},</p>

<p>The Management Ark has created a user profile for you to access the TMA Tour Management Portal. 
    In order to complete the registration process please follow the instructions below within the next 48 hours. 
    If you are reading this message after the link has expired please send an email to The Management Ark to have it resent.
</p>
<p>Your Username/Login Credential is: {{$data['username']}}</p>
<p> Click Link below to complete registration process:<br/>
<p><a href="{{ $data['link']}}">Click here </a> </p>

<p>Thanks,</p>
<p>TMS Team</p>

<p>
    <b>CONFIDENTIALITY NOTICE: </b> <p  style="font-size: 12px;">This e-mail message, including any attachments,
    is for the sole use of the intended recipient(s) and may contain The Management Ark Internal or 
    Confidential information.  Any unauthorized review, use, disclosure or distribution is prohibited. 
    If you are not the intended recipient, please contact the sender by reply e-mail and destroy all copies of the original message. 
</p>
</p>