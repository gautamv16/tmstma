<p>Hi {{ $data['first_name'] }},</p>

<p>Your One Time Verification code: {{$data['code']}}</p>

<p>Thanks,</p>
<p>TMS Team</p>