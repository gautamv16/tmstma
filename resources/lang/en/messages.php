<?php

return [

    'NewUserReg'    =>'New User Registration Initiated',
    'CmpltUserReg'  =>'Completed User Registration',
    'NUserRegFail'  =>'New User Registration Failed',
    'ExUserRegFail'     =>'Existing User Registration Failed',
    'InvldLoginAtmpt'   =>'Invalid Login Attempt -Login attempt by not recognized user',
    'FailUserLogin'     =>'Failed User Login -Failed Login Attempt by known user',
    'Login'     =>'Successful Login Attempt',
    'Logout'    =>'Successful Logout',
    'Timeout'   =>'User Session Timeout',
    'ModPwd'    =>'User Successfully Modified Pword',
    'ModProfile'    =>'User Successfully Modified Profile',
    'ModUserReg'    =>'Modified User Registration Initiated'
];