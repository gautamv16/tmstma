<?php

use Illuminate\Database\Seeder;
use App\Model\UserTypes;

class UserTypeSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $userTypes = [
            ['type' => 'ME Admin', 'code' => 'me_admin'],
            ['type' => 'ME Associates', 'code' => 'me_associates'],
            ['type' => 'Booking Agent', 'code' => 'booking_agent'],
            ['type' => 'Promoter', 'code' => 'promoter'],
            ['type' => 'Musician', 'code' => 'musician'],
            ['type' => 'Client/Band Leader', 'code' => 'client'],
            ['type' => 'Road Manager', 'code' => 'road_manager'],
            ['type' => 'Assistant', 'code' => 'assistant'],
            ['type' => 'Record Co. Exec', 'code' => 'record_exec'],
            ['type' => 'Publicist', 'code' => 'publicist'],
            ['type' => 'Agent', 'code' => 'agent'],
            ['type' => 'Radio Promo', 'code' => 'radio_promo'],
            ['type' => 'Social Netork Promo', 'code' => 'social_network_promo'],
            ];
            
            //'ME Associates', 'Booking Agent', 'Promoter', 'Musician', 'Client/Band Leader', 'Road Manager', 'Assistant', 'Record Co. Exec', 'Publicist', 'Agent', 'Radio Promo', 'Social Netork Promo'];
        foreach ($userTypes as $types) {
            UserTypes::create(['user_type' => $types['type'],'code'=>$types['code']]);
        }
    }

}
